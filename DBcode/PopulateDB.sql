INSERT INTO halland.login (username, password) VALUES ('nuno','123');

INSERT INTO halland.userr (username, password, name, phone, email) VALUES ('nuno','123','Nuno','923 456 748','nuno@gmail.com');

INSERT INTO halland.doctor (username, password, name, phone, email, clinic, location, id, gender, speciality, birthday)
VALUES ('mslopes','1234567','maria','923456789','maria@gmail.com','Hospital do escuro', 'Rua do centro do porto, Porto', 7, 'Female', 'AMAT', '1992-12-26');

INSERT INTO halland.doctor (username, password, name, phone, email, clinic, location, id, gender, speciality, birthday)
VALUES ('DR_joao','drjoao','joao','123456789','joao@gmail.com','Hospital da LUZ', 'Rua do centro de Aveiro, Aveiro', 30, 'Male', 'Olhos', '2021-12-26');

INSERT INTO halland.doctor (username, password, name, phone, email, clinic, location, id, gender, speciality, birthday)
VALUES ('dr_nuno','123','Nuno','145678932','nuno@gmail.com','Centro de Saude Rio Tinto', 'Rua do centro do porto, Porto', 4435, 'Male', 'Pole@bol', '2004-12-26');

INSERT INTO halland.doctor (username, password, name, phone, email, clinic, location, id, gender, speciality, birthday)
VALUES ('zedopipo','maggie','Zé do Pipo','923456789','zedopipo@gmail.com','Hospital de bragança', 'Rua do centro de bragança, Bragança', 4200, 'Male', 'Cardiologista', '2023-12-26');

INSERT INTO halland.patient (username, password, name, phone, email, address, health_number, gender, birthday)
VALUES ('nuno','123','Nuno','923 456 748','nuno@gmail.com','Av...', 4435, 'Other', '2004-05-20');

INSERT INTO halland.pharmacy (username, password, name, phone, email, address, ID)
VALUES ('nuno','123','Nuno','923 456 748','nuno@gmail.com','Av...', '123');

INSERT INTO halland.appointment (code, location, clinic,  date, speciality, time, doctor, patient)
VALUES ('4435','R. Quinta das Freiras, Rio Tinto', 'Centro de Saude Rio Tinto','2004-05-20','Olhos','9 AM', 30, 1234567);

INSERT INTO halland.medicine (name, act_substance)
VALUES ('Ben u ron', 'Paracetamol');

/*Quantity - number of medicine boxes
Patient - Refers to the helth number of a pacient*/
INSERT INTO prescription (dose, duration, quantity, expirationdate, action, patient, medicine, comment, doctor)
VALUES ('2 comp 3x dia','1 mes', 5, '2032-08-09', 'active',4435, 21,'ahsbd', 30);



