package Users;

import java.util.Date;

public class User {
    private String Name;
    private String Phone;
    private String Email;

    public void setUser(String Name, String Phone, String Email) {
        this.Name = Name;
        this.Phone = Phone;
        this.Email = Email;
    }

    public String getName(){
        return Name;
    }


    public String getPhone(){
        return Phone;
    }



    public String getEmail(){
        return Email;
    }



    public void setName(String Name){
        this.Name = Name;
    }

    public void setEmail(String Email){
        this.Email = Email;
    }
    public void setPhone(String Phone){
        this.Phone=Phone;
    }



}
