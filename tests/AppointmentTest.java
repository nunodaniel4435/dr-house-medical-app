import Actions.Appointment;
import Actions.Prescription;
import static org.junit.Assert.*;
import org.junit.Test;

import java.time.LocalDate;

public class AppointmentTest {

    @Test
    public void getDoctor_name() {
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "4 PM";

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);
        assertEquals("Dr_test", app.getDoctor_name());
    }

    @Test
    public void getTime() {
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Location = "Porto";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "4 PM";

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);
        assertEquals("4 PM", app.getTime());
    }

    @Test
    public void getPatient_name() {
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Location = "Porto";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "4 PM";

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);
        assertEquals("App User", app.getPatient_name());
    }

    @Test
    public void timeIsAvailable() {
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Location = "Porto";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "4 PM";

        String [] AvailableTimes = {"9 AM", "10 AM", "11 AM", "12 AM", "2 PM", "3 PM", "4 PM", "5 PM"};

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);

        assertEquals(true, app.TimeIsAvailable(AppTime, AvailableTimes, 8));
        assertEquals(false, app.TimeIsAvailable("6 PM", AvailableTimes, 8));
    }

    @Test
    public void add_prescription() {
        int code = 1;
        String Dose = "2 pills";
        String Dur = "3 days";
        int quant = 2;
        LocalDate exp = LocalDate.now();
        String comment = "with water";
        String drug = "med";
        String doc = "dr_test";
        String pat = "user";
        String Type = "1 time";
        String Act_sub = "medicines";

        Prescription presc = new Prescription(code, Dose, Dur, quant, exp, comment, drug, doc, pat, Type, Act_sub);

        //patient
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Location = "Porto";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "4 PM";

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);
        app.add_prescription(presc);
        assertEquals(1, app.getList().size());
    }

    @Test
    public void stillTodayAppointments() {
        //patient
        LocalDate today = LocalDate.now();
        String Code = "123";
        String Location = "Porto";
        String Clinic = "Tester_Clinic";
        String special = "testing";
        int PatientID = 10;
        String DocName = "Dr_test";
        String PatName = "App User";
        String AppTime = "16:00";

        Appointment app = new Appointment(Code, Clinic, special, today, PatientID, DocName, PatName, AppTime);

        int LocalTime = 14;
        assertEquals(true, Appointment.StillTodayAppointments(app.getTime(), LocalTime));
    }
}