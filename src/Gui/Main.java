package Gui;

import DB.Data_base;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;


public class Main extends Application{

    private String username;
    private String AccountType;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root =FXMLLoader.load(getClass().getResource("login/log in.fxml"));
        primaryStage.setTitle("Halland");
        primaryStage.setScene(new Scene(root, 568,400));
        primaryStage.show();
       // PDDocument doc = new PDDocument();
    }

    public static void main(String[] args) throws SQLException {

        Data_base db = new Data_base();
        db.initialize();
        launch(args);
    }

    public void SetUsername(String username){
        this.username = username;
    }

    public void SetType(String AccountType){
        this.AccountType = AccountType;
    }

    public String getUsername(){
        return this.username;
    }

    public String getAccountType(){
        return this.username;
    }



}
