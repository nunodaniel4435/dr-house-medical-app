package Gui.Patient;
import java.awt.*;
import java.awt.print.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import DB.Data_base;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javax.print.*;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.function.Consumer;
import java.awt.Graphics;


public class SeePrescription_Patient_controller implements Initializable {

    @FXML
    private Label Address_label;

    @FXML
    private Label Date_label;

    @FXML
    private Label Doctor_label;

    @FXML
    private Label Patient_label;

    @FXML
    private Button Print_button, Delete_button;

    @FXML
    private Label active_subs_label;

    @FXML
    private Label commentary_label;

    @FXML
    private Label dose_label;

    @FXML
    private Label duration_label;

    @FXML
    private Label expiration_date_label;

    @FXML
    private Label medicine_label;
    @FXML
    private Label code_label;

    private Consumer<String> callback;

    @FXML
    private Label quantity_label;

    @FXML private ImageView CloseApp;

    @FXML private AnchorPane bar;

    @FXML private GridPane borders;

    static int code_static, user_static;

    public void initialize(URL url, ResourceBundle resourceBundle){
        CloseApp.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });
    }

    public void loadPrescription(int code) throws SQLException {
        code_static = code;
        ResultSet presc = Data_base.getPrescription(code);
        ResultSet patie;
        ResultSet medicine;
        ResultSet doctor;

        LocalDate Data_rs;
        String Data_s;

        while (presc.next()) {
            Data_s = presc.getString("expirationdate");
            quantity_label.setText(String.valueOf(presc.getInt("quantity")));
            dose_label.setText(presc.getString("dose"));
            duration_label.setText(presc.getString("duration"));
            code_label.setText(String.valueOf(" #" + presc.getInt("code")));
            commentary_label.setText(presc.getString("comment"));
            user_static = presc.getInt("patient");
            expiration_date_label.setText(Data_s);
            patie = Data_base.getPatient(presc.getInt("patient"));
            while (patie.next()) {
                Patient_label.setText("                     " + patie.getString("name"));
                Address_label.setText("                      " + patie.getString("address"));
            }
            medicine = Data_base.getMedicine(presc.getInt("medicine"));
            while (medicine.next()) {
                medicine_label.setText("#" +medicine.getInt("code") + " "+ medicine.getString("name") );
                active_subs_label.setText(medicine.getString("act_substance"));
            }
            doctor = Data_base.getDoctor(presc.getInt("doctor"));
            while (doctor.next()) {
                Doctor_label.setText(doctor.getString("name"));
            }
            Data_s = presc.getString("date");
            Date_label.setText(Data_s);
        }

    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }

    public void Delete_button_action(ActionEvent event) throws SQLException {
        callback.accept("delete");
    }


    public void Print_button_action(ActionEvent event) throws PrintException, IOException {
       borders.getStyleClass().remove("borders");
        Print_button.setVisible(false);
       bar.setVisible(false);
        Delete_button.setVisible(false);
        WritableImage snapshot = ((Stage) Print_button.getScene().getWindow()).getScene().snapshot(null);
        Print_button.setVisible(true);
        bar.setVisible(true);
        Delete_button.setVisible(true);
        borders.getStyleClass().add("borders");

        ImageView imageView = new ImageView(snapshot);

        File file = new File("image.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(imageView.snapshot(null,null), null), "png", file);
        } catch (IOException e) {

        }

       Image image = ImageIO.read(new File("image.png"));;

        PrinterJob printJob = PrinterJob.getPrinterJob();

        printJob.setPrintable(new Printable() {
            public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
                if (pageIndex != 0) {
                    return NO_SUCH_PAGE;
                }
                graphics.drawImage(image, 100, 150, 400, 400, null);

                return PAGE_EXISTS;
            }
        });
       try {
            printJob.print();
        } catch (PrinterException e1) {
            //e1.printStackTrace();
        }

        file.delete();

    }





}


