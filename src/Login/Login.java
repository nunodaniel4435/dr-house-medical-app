package Login;

import DB.Data_base;
import Users.Doctor;
import Users.Patient;
import Users.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class Login {

    public static String username;
    private String password;
    public User user=new User();

    public boolean delete;

    public boolean check(String user, String pass) throws SQLException {
        ResultSet rs = Data_base.procura_login(user, pass);

        while (rs.next()){
            if(rs.getString("password").equals(pass)) return true;
            else return false;
        }
          return false;
    }

    public void setUsername(String username) {
        this.username = username;

    }

    public void setPassword(String password) {
        this.password = password;

    }

    public String getUsername() {
        return this.username;

    }

    public String getPassword() {
        return this.password;

    }

    public boolean validate_username_potentiality(String username) throws SQLException {

        if (Data_base.procura_username(username))
            return true;
        else
            return false;
    }

    public boolean novo_registo(Login login, String Clinic, int ID, String Speciality, String Gender, LocalDate Birth_date, String address, String Health_number, String account_type) throws SQLException {

        if(account_type.equals("Doctor")){
           return Data_base.insert_Medic( login.getUsername(),login.getPassword(),login.user.getName(),login.user.getPhone(),login.user.getEmail(),  Clinic,  ID,  Speciality,  Gender,  Birth_date);

        }
        else if(account_type.equals("Patient")){
          return  Data_base.insert_Patient(login.getUsername(),login.getPassword(),login.user.getName(),login.user.getPhone(),login.user.getEmail(),  address,  Health_number,  Gender,  Birth_date);
        }
        else if(account_type.equals("Pharmacy")){
           return Data_base.insert_Pharmacy(login.getUsername(),login.getPassword(),login.user.getName(),login.user.getPhone(),login.user.getEmail(),ID,address);
        }
        else return false;
    }

    public boolean isPatient(String username) throws SQLException {
      ResultSet rs = Data_base.getPatient(username);

      while (rs.next()){
          if(username.equals(rs.getString("username"))) return true;
      }
      return false;
    }

    public boolean isMedic(String username) throws SQLException {
        ResultSet rs = Data_base.getDoctor(username);

        while (rs.next()){
            if(username.equals(rs.getString("username"))) return true;
        }
        return false;
    }

    public boolean isPharmacy(String username) throws SQLException {
        ResultSet rs = Data_base.getPharmacy(username);

        while (rs.next()){
            if(username.equals(rs.getString("username"))) return true;
        }
        return false;
    }


}
