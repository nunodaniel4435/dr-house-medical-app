package Actions;

import java.util.Date;

public class Medicine {

    private final String Name; //obtido no momento da prescrição (atraves do codigo), nao se altera
    private Integer code;
    private String substance;

    public Medicine(String name, Integer code, String substance) {
        /*
         * This is the medicine constructor, that contains all the medicine information
         * @param name - String : name of the medicine
         * @param code - int: code of the medicine (unique)
         * @param substance - String: active substance of the medicine
        */
        Name = name;
        this.code=code;
        this.substance = substance;
    }

    public String getName() {
        return Name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getSubstance() {
        return substance;
    }

    public void setSubstance(String substance) {
        this.substance = substance;
    }
}

