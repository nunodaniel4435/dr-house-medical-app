package Gui.admin;

import DB.Data_base;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class warning_controller implements Initializable{
    @FXML
    private Button confirm;

    @FXML
    private ImageView close;

    @FXML
    private Label message;

    int code;

    int type;

    private Consumer<String> callback;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        close.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });


        confirm.setOnMouseClicked(event -> {

            switch(type){
                case 0:{
                    Data_base.deleteAdmin("patient","health_number",code);
                    break;
                }
                case 1:{
                    Data_base.deleteAdmin("doctor","id",code);
                    break;
                }
                case 2:{
                    Data_base.deleteAdmin("pharmacy","id",code);
                    break;
                }
                case 3:{
                    Data_base.deleteAdmin("medicine","code",code);
                    break;
                }
            }
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();

            callback.accept("ref");
        });
    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }

    public void getData(int code,  int inst) {
        this.code=code;
        this.type= inst;

        switch(type){
            case 0:{
                message.setText("Are you sure you want to delete this patient?");
                break;
            }
            case 1:{
                message.setText("Are you sure you want to delete this doctor?");
                break;
            }
            case 2:{
                message.setText("Are you sure you want to delete this pharmacy?");
                break;
            }
            case 3:{
                message.setText("Are you sure you want to delete this medicine?");
                break;
            }
        }
    }
}


