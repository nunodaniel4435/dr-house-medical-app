package Actions;

import Users.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class Appointment {

    private final String Code;
    String Clinic;
    String Speciality;
    private LocalDate Date;
    private String time;

    private int Patient;
    private String Patient_name;
    private String Doctor_name;
    private int Doctor;

    ArrayList<Prescription> List_Prescription = new ArrayList<Prescription>();;

    public Appointment(String Code,String Clinic, String Speciality, LocalDate Date, int Patient, String Doctor_name, String Patient_name, String Time){
        /*
        * Appointment constructor where a new appointment object is created
        * @param Code: String - Appointment ID
        * @param Clinic: String - Name of the clinic scheduled
        * @param Speciality: String - Speciality of the appointment
        * @param Date: Date - Date of the appointment
        * @param Patient: int - Patient ID
        * @param Doctor_name: String - Name of the doctor
        * @param Patient_name: String - Name of the patient
        * @param Time: String - Hour of the appointment
        */

        this.Code = Code;
        this.Clinic = Clinic;
        this.Speciality = Speciality;
        this.Date=Date;
        this.time = Time;
        this.Patient = Patient;
        //this.Doctor = Doctor;
        this.Doctor_name = Doctor_name;
        this.Patient_name = Patient_name;
    }

    public String getCode(){
        /*
         * Get the appointment code to be identified
         * @return The Appointment Code (identifier)
         */
        return Code;
    }
    public LocalDate getDate(){
        /*
         * Get the appointment date
         * @return The Appointment date
         */
        return Date;
    }
    public int getPatient(){
        /*
         * Get the patient identifier that scheduled that appointment
         * @return The patient ID
         */
        return Patient;
    }
    public int getDoctor(){
        /*
         * Get the doctor identifier
         * @return The doctor ID
         */
        return Doctor;
    }
    public String getDoctor_name(){
        /*
         * Get the doctor name
         * @return The doctor name
         */
        return Doctor_name;
    }
    public String getClinic(){
        /*
         * Get the clinic name
         * @return The clinic where is scheduled the appointment
         */
        return Clinic;
    }

    public String getSpeciality(){
        /*
         * Get the Speciality name
         * @return The Speciality of the appointment
         */
        return Speciality;
    }
    public ArrayList<Prescription> getList(){
        /*
         * Get the prescriptions associated with that appointment
         * @return The list of prescriptions
         */
        return List_Prescription;
    }


    public void setClinic(String Clinic){
        /*
         * Set the clinic name if needed
         * @param Clinic : String - name of the clinic
         */
        this.Clinic = Clinic;
    }
    public void setSpeciality(String Speciality){
        /*
         * Set the Speciality name if needed
         * @param Speciality : String - name of the Speciality
         */
        this.Speciality = Speciality;
    }
    public void setDate(LocalDate Date){
        /*
         * Set the Date name if needed
         * @param Date : LocalDate - new date of the appointment
         */
        this.Date = Date;
    }
    public void setDoctor_name(String Doctor_name){
        /*
         * Set the doctor name if needed
         * @param Doctor_name : String - doctor of the appointment
         */
        this.Doctor_name = Doctor_name;
    }

    public void setDoctor(int Doctor){
        /*
         * Set the doctor id if needed
         * @param Doctor : int - doctor of the appointment
         */
        this.Doctor = Doctor;
    }
    public void add_prescription(Prescription Prescription){
        /*
         * Add a new prescription associated with the appointment
         * @param  : Prescription - object with all the prescription information
         */
        List_Prescription.add(Prescription);
    }
    public void remove_prescription(Prescription Prescription){
        /*
         * remove a prescription associated with the appointment
         * @param  : Prescription - object with all the prescription information
         */
        List_Prescription.remove(Prescription);
    }
    public void remove_all_prescriptions(){
        /*
         * remove all the prescriptions associated with the appointment
         */
        List_Prescription.clear();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPatient_name() {
        return Patient_name;
    }

    public void setPatient_name(String patient_name) {
        Patient_name = patient_name;
    }

    public boolean TimeIsAvailable(String ChooseTime, String [] TimesAvailables, int nTimes){
        /*
         * verifies if the choosen time is available for that day, doctor, clinic, specialty.
         * @param ChooseTime: String - Choosen Time
         * @param TimesAvailables: String[] - all the available times for that day
         * @param nTimes: int - length of the TimesAvailable vector
         * @return true if the time is available and tha appointment can be scheduled
         */
        int i;
        Boolean return_value = false;

        for(i = 0; i<nTimes; i++){
            return_value = ChooseTime.equals(TimesAvailables[i]);
            if(return_value == true) break;
        }

        return return_value;
    }

    public static boolean StillTodayAppointments(String CalendarTime, int LocalTime){
        /*
         * verifies if a certain appointment is yet to be realized today or not
         * @param CalendarTime: String - scheduled hour
         * @param LocalTime: int - local hour
         * @return true if the time is not yet passed
         */
        int cal_time;

        String split[] = CalendarTime.split(":", 2);
        cal_time = Integer.parseInt(split[0]);


        if(cal_time > LocalTime){
            return true;
        }
        return false;
    }
}