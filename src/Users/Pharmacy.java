package Users;

import java.util.ArrayList;

public class Pharmacy extends User{

    private String Address;
    private int id;
    private ArrayList<Patient> array_patients= new ArrayList<Patient>();

    public Pharmacy(Integer ID,String Address){
        this.id = ID;
        this.Address = Address;
    }

    public Pharmacy(int ID, String Name, String Phone, String Email, String Address){
        this.id=ID;
        super.setName(Name);
        super.setPhone(Phone);
        super.setEmail(Email);
        this.Address=Address;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
