package Gui.login;


import Gui.Stage_controller;
import Login.Login;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.event.ActionEvent;

import java.io.IOException;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Logic_controller implements Initializable {

    private Login login_stats = new Login();
    private Stage_controller new_stage = new Stage_controller();

    @FXML
    private ImageView close;

    @FXML
    private TextField username, password;

    @FXML
    private Label labelMessage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        labelMessage.setVisible(false);
        close.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });
    }

    public void login_button_action(ActionEvent event) throws IOException, SQLException {

        if(username.getText().isBlank() == false && password.getText().isBlank()==false){
            validate_login(event);
        }
        else {
            labelMessage.setVisible(true);
            labelMessage.setText("   Please enter username and password.");
        }
    }


    public void register_button_action(ActionEvent event) {

        try {
            new_stage.setStage("/Gui/login/registration.fxml","Dr house", 568,400, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ((Node)(event.getSource())).getScene().getWindow().hide();
        new_stage.showStage();
    }

    public void hidelabel(){
        labelMessage.setVisible(false);
    }


    public void validate_login(ActionEvent event) throws IOException, SQLException {
        if (!login_stats.check(username.getText(), password.getText())){
            labelMessage.setVisible(true);
            labelMessage.setText("Try again. Wrong password or username.");
        }
        else{
           login_stats.setUsername(username.getText());
           if(login_stats.isPatient(username.getText())) {
               new_stage.setStage("/Gui/Patient/menu_patient.fxml", "", 1080, 720, true);
           }
          else if(login_stats.isMedic(username.getText()))
               new_stage.setStage("/Gui/Doctor/menu_doctor.fxml", "", 1080, 720, true);

          else if(login_stats.isPharmacy(username.getText()))
               new_stage.setStage("/Gui/pharmacy/menu_pharmacy.fxml", "", 1080, 720, true);

          else if(username.getText().equals("admin")) {
               new_stage.setStage("/Gui/admin/menu_admin.fxml", "", 1080, 720, true);
          }
           new_stage.showStage();
           Stage login = (Stage) ((Node)(event.getSource())).getScene().getWindow();
           login.close();
       }

    }
}
