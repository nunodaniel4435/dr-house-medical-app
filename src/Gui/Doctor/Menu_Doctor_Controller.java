package Gui.Doctor;

import Actions.Appointment;
import Actions.Medicine;
import Actions.Prescription;
import DB.Data_base;
import Gui.Stage_controller;
import Login.Login;
import Users.Patient;
import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Menu_Doctor_Controller implements Initializable {

    int DoctorId;
    private Stage_controller new_stage = new Stage_controller();
    @FXML
    private Label Welcome_text;
    @FXML
    private Label diary_date;
    @FXML
    private Pane Appointments_pane;

    @FXML
    private ImageView CloseApp;



    @FXML
    private Pane Drugs_pane;

    @FXML
    private Pane MainMenu_pane;

    @FXML
    private Pane Patients_Pane;

    @FXML
    private Pane Profile_pane;

    @FXML
    private JFXButton b_a;

    @FXML
    private JFXButton b_m;

    @FXML
    private JFXButton b_menu;

    @FXML
    private JFXButton b_p;

    @FXML
    private JFXButton b_pro, new_prescrp_button;


    @FXML
    private AnchorPane patient, prescription;

    @FXML
    private TableView<Prescription> TablePrescriptions;
    @FXML
    private TableColumn<Prescription, Integer> presc_code;
    @FXML
    private TableColumn<Prescription, Integer> quantity_m;
    @FXML
    private TableColumn<Prescription, LocalDate> expdate;
    @FXML
    private TableColumn<Prescription, String> name_m;
    @FXML
    private TableColumn<Prescription, Void> presc_btn = new TableColumn<>("Open");

    @FXML
    private TableColumn<Prescription, String> type_col;

    //tabela appointments
    @FXML
    private TableView<Appointment> TabelaAppointments;
    @FXML
    private TableColumn<Appointment, String> Col_Paciente;
    @FXML
    private TableColumn<Appointment, String> Col_especialidade;
    @FXML
    private TableColumn<Appointment, String> Col_Time;
    ObservableList<Appointment> listApp;
    ObservableList<Prescription> listPres;

    //tabela appointments completo
    @FXML
    private TableView<Appointment> TabelaAppointments_complete;
    @FXML
    private TableColumn <Appointment, LocalDate> Col_Date_app;
    @FXML
    private TableColumn <Appointment, LocalDate> Col_Time_app;
    @FXML
    private TableColumn <Appointment, LocalDate> Col_esp_app;
    @FXML
    private TableColumn <Appointment, LocalDate> Col_pat_app;
    ObservableList<Appointment> listAppAll;

    @FXML
    private TableView<Patient> TablePatient;
    @FXML
    private TableColumn<Patient, String> p_name, p_phone;
    @FXML
    private TableColumn<Patient, Integer> p_hnum;
    @FXML
    private TableColumn<Patient, Void> pat_button = new TableColumn<>("See Prescriptions");
    private ObservableList<Patient> data_p;

    /*** MEDICINE TABLE ***/
    @FXML
    private TableView<Medicine> TableMedicine;
    @FXML
    private TableColumn<Medicine, String> m_name, m_acts;
    @FXML
    private TableColumn<Medicine, Integer>  m_code;
    public ObservableList<Medicine> data_m;

    /************  Profile variables  **************/
    @FXML
    private ImageView Pass_Wrong, Pass_Confirm, Name_Wrong, Name_Confirm, Number_Confirm, Number_Wrong, Email_Confirm, Email_Wrong, Address_Confirm, Address_Wrong;
    @FXML
    private PasswordField password, password_conf;
    @FXML
    private ImageView Name_Change, Number_Change, Email_Change, Address_Change, Pass_Change;
    @FXML
    private TextField clinic_text, email_text, number_text, name_text;
    @FXML
    private Label username_text, LabelCheckPass, LabelPassInfo, LabelNumberInfo;
    private String newName, newPass, newNumber, newEmail, newClinic;
    private Boolean phoneOK;
    private int Patient_id;

    /***********  end of Profile variables  ********/
    @FXML
    private Button logout;

    @FXML
    private TextField tf_presc,tf_pat, tf_app, tf_med;

    @FXML
    private Label pat_name;

    int pID;
    int UserId;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SetAllVisible(false);
        MainMenu_pane.setVisible(true);

        LocalDate today = LocalDate.now();
        today.getDayOfWeek().toString();
        String today_str = today.toString();
        diary_date.setText(today.getDayOfWeek().toString()+ " " + today_str);
        try {
            table();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        table_p();
        tablemeds();
        searchbar();
        setallbuttons();

        CloseApp.setOnMouseClicked(event -> {
            System.exit(0);
        });

        logout.setOnMouseClicked(event -> {
            try{
                new_stage.setStage("login/log in.fxml", "Halland", 568, 400, false);
            } catch(IOException e){
                e.printStackTrace();
            }

            new_stage.showStage();
            Stage menu = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            menu.close();
        });
    }

    public void goToAppointmentOnAction(Event event) {
        SetAllVisible(false);
        Appointments_pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_a, "pressed");

        if(prescription.isVisible()){
            undo();
            goToAppointmentOnAction(event);
        }
    }

    public void goToDrugsOnAction(Event event) {
        SetAllVisible(false);
        Drugs_pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_m, "pressed");

        if(prescription.isVisible()){
            undo();
            goToDrugsOnAction(event);
        }
    }

    public void goToMenuOnAction(Event event) {
        SetAllVisible(false);
        MainMenu_pane.setVisible(true);
        setallbuttons();
        changeStyleCLass(b_menu, "pressed");

        if(prescription.isVisible()){
            undo();
            goToMenuOnAction(event);
        }
    }

    public void refresh_drugs(ActionEvent event) {
        tablemeds();
        searchbar();
    }

    ResultSet rs=null;

    public void table() throws SQLException, ParseException {
        Col_Paciente.setCellValueFactory(new PropertyValueFactory<>("Patient_name"));
        Col_especialidade.setCellValueFactory(new PropertyValueFactory<>("speciality"));
        Col_Time.setCellValueFactory(new PropertyValueFactory<>("time"));

        Col_pat_app.setCellValueFactory(new PropertyValueFactory<>("Patient_name"));
        Col_esp_app.setCellValueFactory(new PropertyValueFactory<>("speciality"));
        Col_Time_app.setCellValueFactory(new PropertyValueFactory<>("time"));
        Col_Date_app.setCellValueFactory(new PropertyValueFactory<>("Date"));

        rs = Data_base.getDoctor_user(Login.username);
        rs.next();
        DoctorId = rs.getInt("id");
        listApp = getDoctorAppointment(DoctorId);
        listAppAll = getDoctorAppointmentsAll(DoctorId);

        TabelaAppointments.setItems(listApp);
        TabelaAppointments_complete.setItems(listAppAll);
        fillProfileFieldsFromDB();

        String name;
        rs = Data_base.getDoctor(DoctorId);
        rs.next();
        name = rs.getString("name");
        String split [] = name.split(" ", 2);
        name = split[0];
        Welcome_text.setText("Welcome Back, " + name + "!");
    }

    public void SetAllVisible(boolean b) {
        MainMenu_pane.setVisible(b);
        Appointments_pane.setVisible(b);
        Profile_pane.setVisible(b);
        Drugs_pane.setVisible(b);
        Patients_Pane.setVisible(b);
    }

    public void changeStyleCLass(Button btn, String n_class){
        btn.getStyleClass().clear();
        btn.getStyleClass().add(n_class);
    }

    public void setallbuttons(){
        changeStyleCLass(b_menu,"menu");
        changeStyleCLass(b_p,"menu");
        changeStyleCLass(b_a,"menu");
        changeStyleCLass(b_m,"menu");
        changeStyleCLass(b_pro,"menu");
        new_prescrp_button.setDisable(true);
    }


    public static ObservableList<Appointment> getDoctorAppointmentsAll(int user) throws SQLException, ParseException {
        ObservableList<Appointment> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;
        ResultSet doc, pat;
        LocalDate today = LocalDate.now();
        String today_str = today.toString();
        ResultSet rs = Data_base.getDoctorAppointments(user, today_str, false);

        while (rs.next()) {
            Data_s = rs.getString("Date");
            Data_rs = LocalDate.parse(Data_s);
            doc = Data_base.getDoctor(Integer.parseInt(rs.getString("doctor")));
            doc.next();
            pat = Data_base.getPatient(Integer.parseInt(rs.getString("patient")));
            pat.next();
            list.add(new Appointment(rs.getString("code"), rs.getString("clinic"), rs.getString("speciality"),Data_rs, Integer.parseInt(rs.getString("patient")), doc.getString("name"), pat.getString("name"), rs.getString("time")));
        }
        return list;
    }

    public static ObservableList<Appointment> getDoctorAppointment(int user) throws SQLException, ParseException {
        ObservableList<Appointment> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        int hora = LocalTime.now().getHour();
        String Data_s;
        ResultSet doc, pat;
        LocalDate today = LocalDate.now();
        String today_str = today.toString();
        ResultSet rs = Data_base.getDoctorAppointments(user, today_str, true);


        while (rs.next()) {
            Data_s = rs.getString("Date");
            Data_rs = LocalDate.parse(Data_s);
            doc = Data_base.getDoctor(Integer.parseInt(rs.getString("doctor")));
            doc.next();
            pat = Data_base.getPatient(Integer.parseInt(rs.getString("patient")));
            pat.next();

            String Time = rs.getString("time");
            if(Appointment.StillTodayAppointments(Time, hora)) {
                list.add(new Appointment(rs.getString("code"), rs.getString("clinic"), rs.getString("speciality"),Data_rs, Integer.parseInt(rs.getString("patient")), doc.getString("name"), pat.getString("name"), rs.getString("time")));
            }
        }
        return list;
    }


    /**************************************  PATIENTS **********************************/

    public void refresh(){
        tf_pat.setText("");
        TablePatient.getColumns().remove(3);
        table_p();
        tablemeds();
        searchbar();
    }

    public void refresh_presc(){
        tf_presc.setText("");
        TablePrescriptions.getColumns().remove(5);
        prescTable(pID);
        searchbar_presc();
    }

    public void undo(){
        goToPatientsOnAction();
        TablePrescriptions.getColumns().remove(5);
        new_prescrp_button.setDisable(true);
    }

    public void table_p(){

        p_hnum.setCellValueFactory(new PropertyValueFactory<>("health_number"));
        p_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        p_phone.setCellValueFactory(new PropertyValueFactory<>("phone"));

        try {
            rs = Data_base.getPatient(Login.username);
            while (rs.next()){
                UserId = rs.getInt("health_number");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            data_p = getPacients();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            fillProfileFieldsFromDB();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TablePatient.setItems(data_p);
        addButtontoPat();
    }

    public void tablemeds(){
        m_code.setCellValueFactory(new PropertyValueFactory<>("code"));
        m_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        m_acts.setCellValueFactory(new PropertyValueFactory<>("substance"));

        try {
            data_m = getMedicinesList();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TableMedicine.setItems(data_m);
    }

    public ObservableList<Prescription> getPatPresc(int user) throws SQLException, ParseException {
        ObservableList<Prescription> list = FXCollections.observableArrayList();
        LocalDate Data_rs;
        String Data_s;

        ResultSet rts = Data_base.getPrescriptions(user, "active");

        ResultSet rs1;
        String meds=null;
        while (rts.next()) {
            Data_s = rts.getString("expirationdate");
            Data_rs = LocalDate.parse(Data_s);
            rs1 = Data_base.getMedicine(rts.getInt("medicine"));
            while (rs1.next()){
                meds = rs1.getString("name");
            }
            list.add(new Prescription(rts.getInt("code"), null, null, rts.getInt("quantity"), Data_rs, null, meds, null, null, rts.getString("action"), null));
        }

        return list;
    }

    public void prescTable(int ID){
        presc_code.setCellValueFactory(new PropertyValueFactory<>("Code"));
        quantity_m.setCellValueFactory(new PropertyValueFactory<>("Quantity"));
        expdate.setCellValueFactory(new PropertyValueFactory<>("Expiration_date"));
        name_m.setCellValueFactory(new PropertyValueFactory<>("Drug"));
        type_col.setCellValueFactory(new PropertyValueFactory<>("Type"));

        try {
            listPres = getPatPresc(ID);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TablePrescriptions.setItems(listPres);
        addButtontoPresc();
    }

    public void new_prescription_action(Event event) throws IOException {

        FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/Gui/Doctor/create_prescription.fxml"));

        Parent root = fXMLLoader.load();
        createPrescriptionControll controller = fXMLLoader.getController();

        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root,600,595));
        stage.show();

        controller.setup(
                (String value)->{if(value.equals("create")){
                    refresh_presc();
                }
                });
        controller.getPatient(Patient_id);

    }

    private void openPrescription(int code){
        FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/Gui/Doctor/see_prescription.fxml"));
        try {
            Parent root = fXMLLoader.load();
            prescription_controller controller = fXMLLoader.getController();

            controller.loadPrescription(code);
            controller.setup(
                    (String value)->{if(value.equals("ref")){
                        refresh_presc();
                    }
                    });

            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(root,600,595));
            stage.show();

        } catch (IOException | SQLException ex) {
            Logger.getLogger(prescription_controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addButtontoPat() {
        Callback<TableColumn<Patient, Void>, TableCell<Patient, Void>> cellFactory = new Callback<TableColumn<Patient, Void>, TableCell<Patient, Void>>() {
            @Override
            public TableCell<Patient, Void> call(final TableColumn<Patient, Void> param)  {
                final TableCell<Patient, Void> cell = new TableCell<Patient, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/out.png").toExternalForm());
                    private final JFXButton btn1 = new JFXButton("");
                    {
                        btn1.setOnAction((ActionEvent event) -> {
                            Patient data = getTableView().getItems().get(getIndex());
                            pID=data.getHealth_number();
                            patient.setVisible(false);
                            prescTable(pID);
                            searchbar_presc();
                            prescription.setVisible(true);
                            new_prescrp_button.setDisable(false);
                            Patient_id = data.getHealth_number();
                            pat_name.setText(data.getPatient_name());
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn1, "btn_table");
                            btn1.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn1.setGraphic(del);
                            btn1.setPrefSize(30,30);
                            HBox hbox = new HBox(20,btn1);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        pat_button.setCellFactory(cellFactory);
        pat_button.setPrefWidth(176);
        TablePatient.getColumns().add(pat_button);
    }

    public void addButtontoPresc() {
        Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>> cellFactory = new Callback<TableColumn<Prescription, Void>, TableCell<Prescription, Void>>() {
            @Override
            public TableCell<Prescription, Void> call(final TableColumn<Prescription, Void> param)  {
                final TableCell<Prescription, Void> cell = new TableCell<Prescription, Void>() {
                    ImageView del = new ImageView(getClass().getResource("/images/presc.png").toExternalForm());
                    private final JFXButton btn0 = new JFXButton("");
                    {
                        btn0.setOnAction((ActionEvent event) -> {
                            Prescription data = getTableView().getItems().get(getIndex());
                            openPrescription(data.getCode());
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            changeStyleCLass(btn0, "btn_table");
                            btn0.setPrefSize(25,30);
                            del.setFitWidth(25);
                            del.setFitHeight(25);
                            btn0.setGraphic(del);
                            btn0.setPrefSize(30,30);
                            HBox hbox = new HBox(20,btn0);
                            hbox.setAlignment(Pos.CENTER);
                            setGraphic(hbox);
                        }
                    }
                };
                return cell;
            }
        };

        presc_btn.setCellFactory(cellFactory);
        presc_btn.setPrefWidth(100);
        TablePrescriptions.getColumns().add(presc_btn);
    }

    public void searchbar(){
        FilteredList<Patient> filt_datap = new FilteredList<>(data_p, b->true);
        tf_pat.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datap.setPredicate(patient -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_p = newValue.toLowerCase();
                if(patient.getName().toLowerCase().indexOf(lcf_p) !=-1){
                    return true;
                } else if(String.valueOf(patient.getHealth_number()).indexOf(lcf_p) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Patient> sort_p = new SortedList<>(filt_datap);
        sort_p.comparatorProperty().bind(TablePatient.comparatorProperty());
        TablePatient.setItems(sort_p);

        FilteredList<Appointment> filt_dataapp = new FilteredList<>(listAppAll, b->true);
        tf_app.textProperty().addListener((observable, oldValue, newValue)->{
            filt_dataapp.setPredicate(appointment -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_p = newValue.toLowerCase();
                if(appointment.getPatient_name().toLowerCase().indexOf(lcf_p) !=-1){
                    return true;
                } else if(String.valueOf(appointment.getDate()).indexOf(lcf_p) != -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Appointment> sort_app = new SortedList<>(filt_dataapp);
        sort_app.comparatorProperty().bind(TabelaAppointments_complete.comparatorProperty());
        TabelaAppointments_complete.setItems(sort_app);

        FilteredList<Medicine> filt_datam = new FilteredList<>(data_m, b->true);
        tf_med.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datam.setPredicate(medicine -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_m = newValue.toLowerCase();
                if(medicine.getName().toLowerCase().indexOf(lcf_m) !=-1){
                    return true;
                } else if(String.valueOf(medicine.getCode()).indexOf(lcf_m) != -1){
                    return true;
                } else return false;
            });
        });
        SortedList<Medicine> sort_m = new SortedList<>(filt_datam);
        sort_m.comparatorProperty().bind(TableMedicine.comparatorProperty());
        TableMedicine.setItems(sort_m);
    }

    public void searchbar_presc(){
        FilteredList<Prescription> filt_datap = new FilteredList<>(listPres, b->true);
        tf_presc.textProperty().addListener((observable, oldValue, newValue)->{
            filt_datap.setPredicate(prescription -> {
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lcf_presc = newValue.toLowerCase();
                if((prescription.getDrug().toLowerCase().indexOf(lcf_presc)) !=-1){
                    return true;
                } else if(String.valueOf(prescription.getExpiration_date()).toLowerCase().indexOf(lcf_presc)!= -1){
                    return true;
                }
                else return false;
            });
        });
        SortedList<Prescription> sort_presc = new SortedList<>(filt_datap);
        sort_presc.comparatorProperty().bind(TablePrescriptions.comparatorProperty());
        TablePrescriptions.setItems(sort_presc);
    }

    public void goToPatientsOnAction() {
        SetAllVisible(false);
        Patients_Pane.setVisible(true);
        patient.setVisible(true);
        prescription.setVisible(false);
        setallbuttons();
        changeStyleCLass(b_p, "pressed");
    }

    public void goToProfileOnAction(Event event) {
        SetAllVisible(false);
        Profile_pane.setVisible(true);
        setIconsVisible(false);
        setFieldsDisable(true);
        password.setDisable(true);
        password_conf.setDisable(true);
        setallbuttons();
        changeStyleCLass(b_pro, "pressed");

        if(prescription.isVisible()){
            undo();
            goToProfileOnAction(event);
        }
    }

    public ObservableList<Patient> getPacients() throws SQLException, ParseException{
        ObservableList<Patient> data =FXCollections.observableArrayList();

        ResultSet rs = Data_base.getStuff("SELECT health_number,name,phone,email,address,birthday FROM halland.patient order by health_number");

        LocalDate birth;
        String birth_s;
        Integer aux;

        while(rs.next()){
            birth_s = rs.getString("birthday");
            birth = LocalDate.parse(birth_s);
            aux=Integer.parseInt(rs.getString("health_number"));
            data.add(new Patient(aux, rs.getString("name"), rs.getString("phone"),
                    rs.getString("email"),rs.getString("address"),birth));
        }

        return data;
    }

    public ObservableList<Medicine> getMedicinesList() throws SQLException, ParseException {
        ObservableList<Medicine> data = FXCollections.observableArrayList();

        rs = Data_base.getStuff("SELECT * FROM halland.medicine");
        int aux;

        while(rs.next()){
            aux=Integer.parseInt(rs.getString("code"));
            data.add(new Medicine(rs.getString("name"),aux, rs.getString("act_substance")));
        }

        rs=null;
        return data;
    }


    /*********************************** Profile Stuff ************************************/

    public void ProfileChangeName() throws SQLException {
        CancelChanges();
        name_text.setDisable(false);
        Name_Change.setVisible(false);
        Name_Confirm.setVisible(true);
        Name_Wrong.setVisible(true);
    }

    public void ProfileChangePassword() throws SQLException {
        CancelChanges();
        password.setDisable(false);
        password_conf.setDisable(false);
        Pass_Change.setVisible(false);
        Pass_Wrong.setVisible(true);
        Pass_Confirm.setVisible(true);
        LabelPassInfo.setText("Password between 6 and 15 digits");
        LabelPassInfo.setTextFill(Paint.valueOf("#000000"));
        LabelPassInfo.setVisible(true);
    }

    public void ProfileChangeNumber() throws SQLException {
        CancelChanges();
        number_text.setDisable(false);
        Number_Change.setVisible(false);
        Number_Confirm.setVisible(true);
        Number_Wrong.setVisible(true);
    }

    public void ProfileChangeEmail() throws SQLException {
        CancelChanges();
        email_text.setDisable(false);
        Email_Change.setVisible(false);
        Email_Confirm.setVisible(true);
        Email_Wrong.setVisible(true);
    }

    public void ProfileChangeAddress() throws SQLException {
        CancelChanges();
        Address_Change.setVisible(false);
        Address_Confirm.setVisible(true);
        Address_Wrong.setVisible(true);
        clinic_text.setDisable(false);
    }

    public void CancelChanges() throws SQLException {
        //dont forget to discard changes (dont update atual variables)
        fillProfileFieldsFromDB();
        setIconsVisible(false);
        setFieldsDisable(true);
    }

    public void setIconsVisible(boolean b){
        LabelNumberInfo.setVisible(b);
        LabelCheckPass.setVisible(b);
        LabelPassInfo.setVisible(b);
        Pass_Wrong.setVisible(b);
        Pass_Confirm.setVisible(b);
        Name_Wrong.setVisible(b);
        Name_Confirm.setVisible(b);
        Number_Wrong.setVisible(b);
        Number_Confirm.setVisible(b);
        Email_Wrong.setVisible(b);
        Email_Confirm.setVisible(b);
        Address_Wrong.setVisible(b);
        Address_Confirm.setVisible(b);
        Name_Change.setVisible(!b);
        Email_Change.setVisible(!b);
        Number_Change.setVisible(!b);
        Address_Change.setVisible(!b);
        Pass_Change.setVisible(!b);
    }

    public void setFieldsDisable(boolean b){
        password.setDisable(b);
        password_conf.setDisable(b);
        email_text.setDisable(b);
        number_text.setDisable(b);
        name_text.setDisable(b);
        clinic_text.setDisable(b);
    }

    public void fillProfileFieldsFromDB() throws SQLException {
        String sql;

        sql = "select * from halland.doctor where username = '" + Login.username + "'";
        rs = Data_base.getStuff(sql);
        rs.next();

        name_text.setText(rs.getString("name"));
        newName = name_text.getText();
        password.setText(rs.getString("password"));
        newPass = password.getText();
        password_conf.setText(password.getText());
        number_text.setText(rs.getString("phone"));
        newNumber = number_text.getText();
        email_text.setText(rs.getString("email"));
        newEmail = email_text.getText();
        clinic_text.setText(rs.getString("clinic"));
        newClinic = clinic_text.getText();
        username_text.setText(Login.username);
    }

    public void confirmChanges() throws SQLException {
        String name, pass, passConfirmation, phone, email, clinic;
        String first_name;
        Boolean sucess = false;

        name = name_text.getText();
        pass = password.getText();
        passConfirmation = password_conf.getText();
        phone = number_text.getText();
        email = email_text.getText();
        clinic = clinic_text.getText();

        if(!name_text.isDisable() && name.length() > 2){
            newName = name;
            String split [] = name.split(" ", 2);
            first_name = split[0];
            Welcome_text.setText("Welcome Back, " + first_name + "!");
            sucess = true;
        }
        else if(!password.isDisable()){
            if(checkPass(pass, passConfirmation)){
                newPass = pass;
                sucess = true;
            }
        }
        else if(!number_text.isDisable() && phoneOK){
            newNumber = phone;
            sucess = true;
        }
        else if(!email_text.isDisable()){
            newEmail = email;
            sucess = true;
        }
        else if(!clinic_text.isDisable()){
            newClinic = clinic;
            sucess = true;
        }
        if(sucess){
            setFieldsDisable(true);
            setIconsVisible(false);
            UpdateProfileOnDB();
        }

    }

    private boolean checkPass(String Pass_field, String PassConf_field){
        boolean retVal = true;

        if(Pass_field.length() < 6 || Pass_field.length() > 15){
            LabelPassInfo.setVisible(true);
            LabelPassInfo.setTextFill(Paint.valueOf("#eb0404"));
            LabelCheckPass.setVisible(false);
            retVal = false;
        }
        if(!Pass_field.equals(PassConf_field)){
            if(retVal) {
                LabelPassInfo.setVisible(false);
            }
            LabelCheckPass.setVisible(true);
            LabelCheckPass.setText("Passwords don't match!");
            LabelCheckPass.setTextFill(Paint.valueOf("#eb0404"));
            retVal = false;
        }
        return retVal;
    }

    public void phone_check(KeyEvent event){
        if (!((int) event.getCharacter().charAt(0) >= 48 && (int) event.getCharacter().charAt(0) < 58)) {
            number_text.deletePreviousChar();
        }
        if(number_text.getText().length()>=9){
            phoneOK = true;
            LabelNumberInfo.setVisible(false);
        }
        else{
            phoneOK = false;
            LabelNumberInfo.setVisible(true);
            LabelNumberInfo.setText("Must contain at least 9 numbers");
            LabelNumberInfo.setTextFill(Paint.valueOf("#eb0404"));
        }
    }

    public void UpdateProfileOnDB() throws SQLException {
        String sql;

        //update the fields of patient
        sql = "UPDATE halland.doctor SET name='" + newName + "',password='" + newPass + "',phone='" + newNumber + "',email='" + newEmail + "',clinic='" + newClinic+ "' where username='" + Login.username + "'";
        Data_base.updateDBfields(sql);

        sql = "UPDATE halland.users SET name='" + newName + "',password='" + newPass + "',phone='" + newNumber + "',email='" + newEmail + "' where username='" + Login.username + "'";
        Data_base.updateDBfields(sql);
    }

}
