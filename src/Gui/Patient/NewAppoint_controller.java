package Gui.Patient;

import Login.Login;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import DB.Data_base;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class NewAppoint_controller implements Initializable {
    @FXML
    private ImageView CloseNAppointment;

    private enum AppTimes {
        NINE_AM {
            public String toString() {
                return "09:00";
            }
        },

        TEN_AM {
            public String toString() {
                return "10:00";
            }
        },

        ELEVEN_AM {
            public String toString() {
                return "11:00";
            }
        },

        TWO_PM {
            public String toString() {
                return "14:00";
            }
        },

        THREE_PM {
            public String toString() {
                return "15:00";
            }
        },

        FOUR_PM {
            public String toString() {
                return "16:00";
            }
        },

        FIVE_PM {
            public String toString() {
                return "17:00";
            }
        },

        SIX_PM {
            public String toString() {
                return "18:00";
            }
        },

        SEVEN_PM {
            public String toString() {
                return "19:00";
            }
        }
    }

    @FXML
    private ChoiceBox ChooseDoctor, ChooseSpeciallity, ChooseClinic;

    private String Doctor, Speciality, Clinic;
    private LocalDate ChooseDate;
    private String FinalClinic = null;
    private String FinalSpecial = null;
    private String FinalDoctor = null;
    private LocalDate FinalDate = null;
    private String FinalTime = null;

    @FXML
    private DatePicker AppDatePicker;

    @FXML
    private Button B_9AM;
    @FXML
    private Button B_10AM;
    @FXML
    private Button B_11AM;
    @FXML
    private Button B_2PM;
    @FXML
    private Button B_3PM;
    @FXML
    private Button B_4PM;
    @FXML
    private Button B_5PM;
    @FXML
    private Button B_6PM;
    @FXML
    private Button B_7PM;
    @FXML
    private Button RequestButton;
    @FXML
    private Label message;

    private Consumer<String> callback;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        message.setVisible(false);
        try {
            getExistingClinics();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CloseNAppointment.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });
        RequestButton.setOnMouseClicked(event -> {
            if(FinalTime != null){
                try {
                    int PatientID;
                    ResultSet rs = Data_base.getPatient(Login.username);
                    rs.next();
                    PatientID = rs.getInt("health_number");

                    Data_base.CreateAppointment(FinalClinic, FinalSpecial, FinalDoctor, FinalDate, FinalTime, PatientID);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                callback.accept("ref");

                Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
                Close.close();
            }
            else{
                message.setText("Please complete the form!");
                message.setVisible(true);
            }
        });

        ChooseSpeciallity.setOnAction((event) -> {
            int selectedIndex = ChooseSpeciallity.getSelectionModel().getSelectedIndex();
            Object selectedItem = ChooseSpeciallity.getSelectionModel().getSelectedItem();
            try {
                OnChooseSpeciality();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        ChooseClinic.setOnAction((event) -> {
            int selectedIndex = ChooseClinic.getSelectionModel().getSelectedIndex();
            Object selectedItem = ChooseClinic.getSelectionModel().getSelectedItem();
            try {
                OnChooseClinics();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        ChooseDoctor.setOnAction((event) -> {
            int selectedIndex = ChooseDoctor.getSelectionModel().getSelectedIndex();
            Object selectedItem = ChooseDoctor.getSelectionModel().getSelectedItem();
            OnChooseDoctor();
        });

        setAllButtonDisable(true);
    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }

    public void changeStyleCLass(String n_class){
        message.getStyleClass().clear();
        message.getStyleClass().add(n_class);
    }

    public void getExistingClinics() throws SQLException {
        int nclinics = Data_base.getNclinics();
        String [] clinicas = Data_base.getClinicsNames(nclinics);

        for (int i=0; i < nclinics; i++){
            ChooseClinic.getItems().add(clinicas[i]);
        }
    }

    public void OnChooseClinics() throws SQLException {
        message.setVisible(false);
        if(!ChooseClinic.getSelectionModel().isEmpty()){
            Clinic = ChooseClinic.getValue().toString();
            if(!Clinic.equals(FinalClinic)){
                ChooseSpeciallity.getItems().clear();
                ChooseDoctor.getItems().clear();
                FinalClinic = Clinic;
                getExistingSpecial(Clinic);
            }
        }
    }

    public void OnChooseSpeciality() throws SQLException {
        message.setVisible(false);
        if(!ChooseSpeciallity.getSelectionModel().isEmpty()){
            Speciality = ChooseSpeciallity.getValue().toString();
            if(!Speciality.equals(FinalSpecial)){
                ChooseDoctor.getItems().clear();
                FinalSpecial = Speciality;
                getExistingDoctors(Clinic, Speciality);
            }
        }
    }

    public void OnChooseDoctor(){
        message.setVisible(false);
        if(!ChooseDoctor.getSelectionModel().isEmpty()){
            Doctor = ChooseDoctor.getValue().toString();
            if(!Doctor.equals(FinalDoctor)){
                FinalDoctor = Doctor;
            }
        }
    }

    public void OnChooseDate() throws SQLException {
        LocalDate today = LocalDate.now();
        message.setVisible(false);
        if(AppDatePicker.getValue() != null){
            ChooseDate = AppDatePicker.getValue();

            //the if had !AppDatePicker.getValue().equals(FinalDate) &&
            if(FinalClinic != null && FinalDoctor != null && FinalSpecial != null){
                FinalDate = ChooseDate;
                if((today.isBefore(AppDatePicker.getValue())) || today.isEqual(AppDatePicker.getValue())) {
                    getExistingTimes(Clinic, Speciality, Doctor, ChooseDate);
                }
                else{
                    setAllButtonDisable(true);
                }
            }
        }
    }

    public void getExistingDoctors(String Clinic, String Speciality) throws SQLException {
        int ndoctors = Data_base.getNdoctors(Clinic, Speciality);
        String [] doctors = Data_base.getDoctorsNames(Clinic, Speciality, ndoctors);

        for (int i=0; i < ndoctors; i++){
            ChooseDoctor.getItems().add(doctors[i]);
        }
    }

    public void getExistingSpecial(String Clinic) throws SQLException {
        int nspecial = Data_base.getNspecial(Clinic);
        String [] Specialt = Data_base.getSpecialNames(Clinic, nspecial);

        for (int i=0; i < nspecial; i++){
            ChooseSpeciallity.getItems().add(Specialt[i]);
        }
    }

    public void getExistingTimes(String Clinic, String Speciality, String Doctor, LocalDate ChooseDate) throws SQLException {
        int nTimes = Data_base.getNtimes(Clinic, Speciality, Doctor, ChooseDate);
        String [] Times = Data_base.getAvailableTimes(Clinic, Speciality, Doctor, ChooseDate, nTimes);

        setAllButtonDisable(false);
        for(int i= 0; i<nTimes; i++){
            check_Button(Times[i]);
        }
        if(LocalDate.now().equals(ChooseDate)){
        if(LocalTime.now().getHour() >= 8){
            check_Button("09:00");
        }
        if(LocalTime.now().getHour() >= 9){
            check_Button("10:00");
        }
        if(LocalTime.now().getHour() >= 10){
            check_Button("11:00");
        }
        if(LocalTime.now().getHour() >= 13){
            check_Button("14:00");
        }
        if(LocalTime.now().getHour() >= 14){
            check_Button("15:00");
        }
        if(LocalTime.now().getHour() >= 15){
            check_Button("16:00");
        }

        if(LocalTime.now().getHour() >= 16){
            check_Button("17:00");
        }
        if(LocalTime.now().getHour() >= 17){
            check_Button("18:00");
        }
        if(LocalTime.now().getHour() >= 18){
            check_Button("19:00");
        }
    }}

    void setAllButtonDisable(boolean b){
        B_9AM.setDisable(b);
        B_10AM.setDisable(b);
        B_11AM.setDisable(b);
        B_2PM.setDisable(b);
        B_3PM.setDisable(b);
        B_4PM.setDisable(b);
        B_5PM.setDisable(b);
        B_6PM.setDisable(b);
        B_7PM.setDisable(b);
    }

    void check_Button(String Time){
        if(Time.equals(AppTimes.NINE_AM.toString())){
            B_9AM.setDisable(true);
        }
        else if(Time.equals(AppTimes.TEN_AM.toString())){
            B_10AM.setDisable(true);
        }
        else if(Time.equals(AppTimes.ELEVEN_AM.toString())){
            B_11AM.setDisable(true);
        }
        else if(Time.equals(AppTimes.TWO_PM.toString())){
            B_2PM.setDisable(true);
        }
        else if(Time.equals(AppTimes.THREE_PM.toString())){
            B_3PM.setDisable(true);
        }
        else if(Time.equals(AppTimes.FOUR_PM.toString())){
            B_4PM.setDisable(true);
        }
        else if(Time.equals(AppTimes.FIVE_PM.toString())){
            B_5PM.setDisable(true);
        }
        else if(Time.equals(AppTimes.SIX_PM.toString())){
            B_6PM.setDisable(true);
        }
        else if(Time.equals(AppTimes.SEVEN_PM.toString())){
            B_7PM.setDisable(true);
        }
    }

    public void OnSelect9AM(){
        FinalTime = AppTimes.NINE_AM.toString();
    }
    public void OnSelect10AM(){
        FinalTime = AppTimes.TEN_AM.toString();
    }
    public void OnSelect11AM(){
        FinalTime = AppTimes.ELEVEN_AM.toString();
    }
    public void OnSelect2PM(){
        FinalTime = AppTimes.TWO_PM.toString();
    }
    public void OnSelect3PM(){
        FinalTime = AppTimes.THREE_PM.toString();
    }
    public void OnSelect4PM(){
        FinalTime = AppTimes.FOUR_PM.toString();
    }
    public void OnSelect5PM(){
        FinalTime = AppTimes.FIVE_PM.toString();
    }
    public void OnSelect6PM(){
        FinalTime = AppTimes.SIX_PM.toString();
    }
    public void OnSelect7PM(){
        FinalTime = AppTimes.SEVEN_PM.toString();
    }
}