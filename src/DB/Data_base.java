package DB;

import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;

public  class Data_base {

    static Connection connection;
    static ResultSet rs;
    static PreparedStatement send;
    static  Statement receive;
    static String sql;

    public void initialize() throws SQLException {
         String url =        "jdbc:postgresql://db.fe.up.pt/meec1a0404?user=meec1a0404&password=yxUMgZqq&ssl=false";
        String username = "meec1a0404";
        String password = "yxUMgZqq";
        {
            try {
                connection = DriverManager.getConnection( url);

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Error in connecting to DB");
                return;
            }
        }
        exp_dat_check();

    }

    public static ResultSet procura_login(String username,String password) throws SQLException {

        String sql = "SELECT username, password FROM halland.login WHERE username = '" + username +"'" ;
         rs=receive_db(sql);

         return rs;
    }

    private void exp_dat_check() throws SQLException {

        sql = "SELECT code,action, expirationdate FROM halland.prescription";
        rs=receive_db(sql);

        while(rs.next()){
            if(LocalDate.parse(rs.getString("expirationdate")).isBefore(LocalDate.now()) && rs.getString("action").equals("active")){
                sql = "Update halland.prescription SET action = 'deleted' where code = " + rs.getInt("code");
                send_db(sql);
            }
        }

    }
    public static boolean procura_username(String username) throws SQLException {

        sql = "SELECT username FROM halland.login WHERE username = '" + username +"'" ;
        rs=receive_db(sql);

       while (rs.next()){
           return !username.equals(rs.getString(1));
       }

         return true;
    }

    public static boolean procura_id_unico(int id, String type) throws SQLException {


        if(type.equals("Doctor")){

        sql = "SELECT id FROM halland.doctor WHERE id = " + id ;
        }

        else if(type.equals("Pharmacy")){
            sql = "SELECT id FROM halland.pharmacy WHERE id = " + id ;
        }

        else if(type.equals("Patient")){
            sql = "SELECT health_number FROM halland.patient WHERE health_number = " + id ;
        }
        rs=receive_db(sql);

        while (rs.next()){
            if(!type.equals("Patient"))
            return (id ==rs.getInt("id"));
            else return (id ==rs.getInt("health_number"));
        }

        return false;
    }

    public static boolean insert_Medic(String username, String password, String name,String phone,String email, String Clinic, int ID, String Speciality, String Gender, LocalDate Birth_date) {

         sql = "  INSERT INTO halland.doctor (username, password, name, phone, email, clinic, id, gender, speciality, birthday) " +
                "VALUES ('"+username+"','"+password+"','"+name+"','"+phone+"','"+email+"','"+Clinic+"','"+ID+"' , '"+Gender+"', '"+Speciality+"', '"+Birth_date+"');";
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }
    public static boolean insert_Patient(String username, String password,String name,String phone,String email,  String address, String Health_number, String Gender, LocalDate Birth_date){

        sql = "  INSERT INTO halland.patient (username, password, name, phone, email, address, health_number, gender, birthday) " +
                "VALUES ('"+username+"','"+password+"','"+name+"','"+phone+"','"+email+"','"+address+"','"+Health_number+"' , '"+Gender+"', '"+Birth_date+"');";
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public static boolean insert_Pharmacy(String username, String password,String name,String phone,String email,int ID, String address){

        sql = "  INSERT INTO halland.pharmacy (username, password, name, phone, email, address, ID)" +
                "VALUES ('"+username+"','"+password+"','"+name+"','"+phone+"','"+email+"','"+address+"',' "+ID+"');";
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static int send_db(String sql) throws SQLException {
        send = connection.prepareStatement(sql);
        return send.executeUpdate();
    }

    private static ResultSet receive_db(String sql) throws SQLException {
        receive = connection.createStatement();
        return receive.executeQuery(sql);

    }

    public static int getNclinics() throws SQLException {
        sql = "select distinct clinic from halland.doctor";
         rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public static String [] getClinicsNames(int Nclinics) throws SQLException {
        sql = "select distinct clinic from halland.doctor";
        rs = receive_db(sql);
        String [] clinics = new String[Nclinics];

        int cont = 0;

        while (rs.next()){
            clinics[cont] = new String();
            clinics[cont] = rs.getString(1);
            cont++;
        }
        return clinics;
    }

    public static ResultSet getAppointments(int userID) throws SQLException {
        LocalDate today = LocalDate.now();
        sql = "select * from halland.appointment where patient = " + userID + " and date >= '" + today.toString() + "'order by date, time asc";
        rs = receive_db(sql);

        return rs;
    }
    public static ResultSet getPrescriptions(int userID,String action) throws SQLException {
        sql = "select * from halland.prescription where patient = " + userID + "and action ='" + action+"'" ;
         rs = receive_db(sql);


        return rs;
    }

    public static ResultSet getPrescriptions(int userID,String action,String drug_name) throws SQLException {
       rs= getMedicine(drug_name);
       int medicineCode=0;
       while (rs.next()){
           medicineCode = rs.getInt("code");
       }

        sql = "select * from halland.prescription where patient = " + userID + "and action ='" + action+"' and medicine = '" + medicineCode +"'" ;
        rs = receive_db(sql);


        return rs;
    }

    public static void deletePrescription(int UserId, int Code) throws SQLException {
        sql = "Update halland.prescription SET action = 'deleted' where patient = " + UserId + "and code =" + Code;
         send_db(sql);
         return;
    }

    public static ResultSet getPrescription(int code) throws SQLException {
        sql = "select * from halland.prescription where code = " + code;
        rs = receive_db(sql);

        return rs;
    }
    public static ResultSet getPatient(int userId) throws SQLException {
        sql = "select * from halland.patient where health_number = " + userId;
        rs = receive_db(sql);

        return rs;
    }public static ResultSet getPatient(String username) throws SQLException {
        sql = "select * from halland.patient where username = '" + username + "'";
        rs = receive_db(sql);


        return rs;
    }
    public static ResultSet getDoctor_user(String username) throws SQLException {
        sql = "select * from halland.doctor where username = '" + username + "'";
        rs = receive_db(sql);

        return rs;
    }

    public static ResultSet getDoctor(int doctorId) throws SQLException {
        sql = "select * from halland.doctor where id = " + doctorId;
        rs = receive_db(sql);

        return rs;
    }
    public static ResultSet getDoctor(String username) throws SQLException {
        sql = "select * from halland.doctor where username = '" + username + "'";
        rs = receive_db(sql);

        return rs;
    }
    public static ResultSet getPharmacy(String username) throws SQLException {
        sql = "select username from halland.pharmacy where username = '" + username + "'";
        rs = receive_db(sql);

        return rs;
    }

    public static ResultSet getMedicine(int code) throws SQLException {
        sql = "select * from halland.medicine where code = " + code;
        rs = receive_db(sql);

        return rs;
    }

    public static ResultSet getMedicine(String name) throws SQLException {
        sql = "select * from halland.medicine where name = '" + name + "'";
        rs = receive_db(sql);
        return rs;
   }

    public static String getActive_subs(int code) throws SQLException {
        sql = "select act_substance from halland.medicine where code = " + code;
        rs = receive_db(sql);

        while(rs.next()) return rs.getString("act_substance");

        return null;
    }

    public static String[][] getMedicine() throws SQLException {
        sql = "select * from halland.medicine ORDER by code";
        rs = receive_db(sql);
        int size=0, i=0;

        while(rs.next()) {
            size++;
       }
        rs = receive_db(sql);

        String[][] medicines = new String[size][2];

        while(rs.next()){
           medicines[i][0]=rs.getString(1);
           medicines[i][1]= rs.getString(2);
            i++;
        }

        return medicines;
    }


    public static int getNdoctors(String Clinic, String Speciality) throws SQLException {
        sql = "select distinct name from halland.doctor where clinic = '" + Clinic + "' AND speciality = '" + Speciality + "'";
        ResultSet rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public static String [] getDoctorsNames(String Clinic, String Speciality, int Ndoctors) throws SQLException {
        sql = "select distinct name from halland.doctor where clinic = '" + Clinic + "' AND speciality = '" + Speciality + "'";
        ResultSet rs = receive_db(sql);
        String [] Doctors = new String[Ndoctors];

        int cont = 0;

        while (rs.next()){
            Doctors[cont] = new String();
            Doctors[cont] = rs.getString("name");
            cont++;
        }
        return Doctors;
    }

    public static int getNspecial(String Clinic) throws SQLException {
        sql = "select distinct speciality from halland.doctor where clinic = '" + Clinic + "'";
        ResultSet rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public static String [] getSpecialNames(String Clinic, int Ndoctors) throws SQLException {
        sql = "select distinct speciality from halland.doctor where clinic = '" + Clinic + "'";
        ResultSet rs = receive_db(sql);
        String [] Doctors = new String[Ndoctors];

        int cont = 0;

        while (rs.next()){
            Doctors[cont] = new String();
            Doctors[cont] = rs.getString(1);
            cont++;
        }
        return Doctors;
    }

    public static ResultSet getStuff(String SQL) throws SQLException {
        ResultSet rs = receive_db(SQL);

        return rs;
    }



    public static int getNtimes(String Clinic, String Speciality, String Doctor, LocalDate data) throws SQLException {
        sql = "select id from halland.doctor where name ='" + Doctor + "'"; //get doctor code
        ResultSet rs = receive_db(sql);
        rs.next();
        String DoctorID_s = rs.getString("id");

        sql = "select time from halland.appointment where doctor =" + DoctorID_s + " and date = '" + data.toString() + "'";
        rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public static String [] getAvailableTimes(String Clinic, String Speciality, String Doctor, LocalDate data, int Ntimes) throws SQLException {
        sql = "select id from halland.doctor where name ='" + Doctor + "'"; //get doctor code
        ResultSet rs = receive_db(sql);
        rs.next();
        String DoctorID_s = rs.getString("id");

        sql = "select time from halland.appointment where doctor =" + DoctorID_s + " and date = '" + data.toString() + "'";
        rs = receive_db(sql);

        String [] times = new String[Ntimes];
        int cont = 0;

        while (rs.next()){
            times[cont] = new String();
            times[cont] = rs.getString("time");
            cont++;
        }

        return times;
    }
    public static void CreateAppointment(String clinic, String special, String doctor, LocalDate data, String time, int PatientID) throws SQLException {
        sql = "select id from halland.doctor where name ='" + doctor + "' and clinic ='"+ clinic + "'and speciality ='"+ special + "'"; //get doctor code


        ResultSet rs = receive_db(sql);
        rs.next();
        String DoctorID_s = rs.getString("id");
        String PatientID_s = Integer.toString(PatientID);

        sql = "INSERT INTO halland.appointment ( clinic,  date, speciality, time, doctor, patient) VALUES " +
                "('" + clinic + "','" + data + "','" + special + "','" + time + "','" + DoctorID_s + "','" + PatientID_s + "');";
        send_db(sql);
    }

    public static void CreatePrescription(String dose, String duration, String quantity,
                                             String date, String comment, int drug,int doctor,
                                             int patient    , String exp_date) throws SQLException {

        sql="INSERT INTO halland.prescription (dose, duration, quantity, expirationdate, action," +
                "comment, date, patient, doctor, medicine) VALUES ('"+dose+"','"+duration+"','"+quantity+"'," +
                "'"+exp_date+"','active','"+comment+"','"+date+"','"+patient+"','"+doctor+"','"+drug+"')";

        send_db(sql);
    }

    public static boolean searchmed(String name) throws SQLException{
       sql = "SELECT * FROM halland.medicine WHERE name='"+name+"'";

        ResultSet rs=receive_db(sql);

        while (rs.next()){
            return name.equals(rs.getString("name"));
        }
        return false;
    }

    public static void inssertmed(String name, String actsub) throws SQLException{
       sql="INSERT INTO halland.medicine (name, act_substance) VALUES ('"+name+"','"+actsub+"')";

        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void updateDBfields(String sqlCode) throws SQLException {
       send_db(sqlCode);
    }

    public static void deleteAppoitment(int code) throws SQLException{
        sql = "DELETE FROM halland.appointment WHERE code="+code;
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAdmin(String table, String clause, int value){
        sql = "DELETE FROM halland."+table+" WHERE "+clause+"="+value;
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getPresc_num() throws SQLException{
        sql = "select * from halland.prescription";
        ResultSet rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public static int getApp_num() throws SQLException{
        sql = "select * from halland.appointment";
        ResultSet rs = receive_db(sql);

        int cont = 0;

        while (rs.next()){
            cont++;
        }

        return cont;
    }

    public void updatePresc(int code) {
        sql = "UPDATE halland.prescription SET action='in use' WHERE code="+code;
        try {
            send_db(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet getDoctorAppointments(int userID, String date, boolean today) throws SQLException {
        if(today) {
            sql = "select * from halland.appointment where doctor = " + userID + "and date = '" + date + "' order by time asc";
        }
        else{
            sql = "select * from halland.appointment where doctor = " + userID + "and date >= '" + date + "' order by time asc";
        }

        rs = receive_db(sql);
        return rs;
    }
}
