
module Halland {

    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires com.jfoenix;
    requires java.desktop;
    requires javafx.swing;
    requires org.apache.pdfbox;

    opens Gui;

    opens Actions;
    opens Users;
    opens Gui.login;
    opens Gui.Patient;
    opens Gui.admin;
    opens Gui.Doctor;
    opens Gui.pharmacy;
}