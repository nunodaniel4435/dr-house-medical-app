package Gui.Doctor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import DB.Data_base;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.function.Consumer;

public class prescription_controller implements Initializable {

    @FXML
    private Label Address_label;

    @FXML
    private Label Date_label;

    @FXML
    private Label Doctor_label;

    @FXML
    private Label Patient_label;


    @FXML
    private Label active_subs_label;

    @FXML
    private Label commentary_label;

    @FXML
    private Label dose_label;

    @FXML
    private Label duration_label;

    @FXML
    private Label expiration_date_label;

    @FXML
    private Label medicine_label;
    @FXML
    private Label code_label;

    @FXML
    private Label quantity_label;

    @FXML
    private ImageView CloseApp;

    Data_base db = new Data_base();

    static int code_static, user_static;

    private Consumer<String> callback;

    public void initialize(URL url, ResourceBundle resourceBundle){
        CloseApp.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });

    }

    public void loadPrescription(int code) throws SQLException {
        code_static = code;
        ResultSet presc = db.getPrescription(code);
        ResultSet patie;
        ResultSet medicine;
        ResultSet doctor;

        LocalDate Data_rs;
        String Data_s;

        while (presc.next()) {
            Data_s = presc.getString("expirationdate");
            Data_rs = LocalDate.parse(Data_s);
            quantity_label.setText(String.valueOf(presc.getInt("quantity")));
            dose_label.setText(presc.getString("dose"));
            duration_label.setText(presc.getString("duration"));
            code_label.setText(String.valueOf(" #" + presc.getInt("code")));
            commentary_label.setText(presc.getString("comment"));
            user_static = presc.getInt("patient");
            expiration_date_label.setText(Data_s);
            patie = db.getPatient(presc.getInt("patient"));
            while (patie.next()) {
                Patient_label.setText("                     " + patie.getString("name"));
                Address_label.setText("                      " + patie.getString("address"));
            }
            medicine = db.getMedicine(presc.getInt("medicine"));
            while (medicine.next()) {
                medicine_label.setText("#" +medicine.getInt("code") + " "+ medicine.getString("name") );
                active_subs_label.setText(medicine.getString("act_substance"));
            }
            doctor = db.getDoctor(presc.getInt("doctor"));
            while (doctor.next()) {
                Doctor_label.setText(doctor.getString("name"));
            }
            Date_label.setText(LocalDate.now().toString());
        }

    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }


}


