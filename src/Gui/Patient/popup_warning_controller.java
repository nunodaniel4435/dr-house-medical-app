package Gui.Patient;

import DB.Data_base;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class popup_warning_controller implements Initializable {
    @FXML
    private Button confirm;

    @FXML
    private ImageView close;

    @FXML
    private Label message;

    int id_u, id_p, code_a;

    int type;
    Stage prev_stage;

    private Consumer<String> callback;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        close.setOnMouseClicked(event -> {
            Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
            Close.close();
        });


        confirm.setOnMouseClicked(event -> {
            Data_base db = new Data_base();
            if(type!=2){
                try {
                    db.deletePrescription(id_u,id_p);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
                Close.close();

                if(prev_stage!=null){
                    prev_stage.close();
                }
            }
            else{
                try {
                    db.deleteAppoitment(code_a);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Stage Close = (Stage) ((Node)(event.getSource())).getScene().getWindow();
                Close.close();
            }
            callback.accept("delete");
        });
    }

    public void setup(Consumer<String> callback) {
        this.callback = callback;
    }

    public void getData(int ID_user, int ID_presc, int ID_app, int inst, Stage prev) {
        this.id_u=ID_user;
        this.id_p=ID_presc;
        this.type=inst;
        this.prev_stage=prev;
        this.code_a= ID_app;

        switch(type){
            case 0:{
                message.setText("Are you sure you want to delete medicine?");
                break;
            }
            case 1:{
                message.setText("Are you sure you want to delete prescription?");
                break;
            }
            case 2:{
                message.setText("Are you sure you want to delete appointment?");
                break;
            }
        }
    }
}
