package Users;

import java.text.DateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Doctor extends User{
    private String Clinic,Specialty,Gender;
    private int Id;
    private LocalDate Birth_date;

    public Doctor(String Clinic, int Id, String Specialty,String Gender,  LocalDate birth){
        this.Clinic = Clinic;
        this.Id = Id;
        this.Specialty = Specialty;
        this.Gender= Gender;
        this.Birth_date = birth;
    }

    public Doctor(int ID, String Name, String phone, String email, String speciality, String clinic){
        /*
         * This is the doctor constructor, that contains all the doctor information
         * @param ID - int : id (unique) of the doctor
         * @param Name - String : Doctor's name
         * @param phone - String : Doctor's phone number
         * @param email - String : Doctor's email
         * @param speciality - String : Doctor's speciality
         * @param clinic - String : clinic which the doctor is associated
         */
        this.Id=ID;
        super.setName(Name);
        super.setPhone(phone);
        super.setEmail(email);
        this.Specialty=speciality;
        this.Clinic=clinic;
    }

    public String getClinic(){
        return Clinic;
    }

    public int getId(){
        return Id;
    }

    public String getSpeciality(){
        return Specialty;
    }

    public String getGender() {
        return Gender;
    }

    public LocalDate getBirth_date() {
        return Birth_date;
    }

    public void setClinic(String Clinic){
        this.Clinic=Clinic;
    }

    public void setId(int Id){
        this.Id = Id;
    }

    public void setSpeciality(String Speciality){
        this.Specialty = Speciality;
    }


    public void setBirth(LocalDate birth) {
        this.Birth_date = birth;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }
}
