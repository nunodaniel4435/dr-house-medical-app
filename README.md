<!-- ABOUT THE PROJECT -->
## Dr house
Project of a Medical Prescription System made for the class of Projeto de Software (PSW) in the Major of Eletrotecnical and Computeer Engeenering.
It was developed in Windows.

### Users
It can be used by Patients, Doctors and Pharmacies independently. For each user type, it has differente functionalitys that
makes this app appealing for everyone. It is also very user friendly, with very intuitive interaction, so that everybody can use it propertly.

### Running
This application is build in JAVA and **needs a VPN connection to FEUP.net** so that it has acess to the DataBase.

If you want to run the application, please install  [Java RunTime Environment](https://docs.oracle.com/goldengate/1212/gg-winux/GDRAD/java.htm#BGBFJHAB). Now, download the [RunHallandApp](https://gitlab.com/psw2122/a04/halland/-/tree/master/RunHallandApp) folder and open the Halland batch file. The only thing left now it to you to create an account!!  
 

### Built With

List of major frameworks/libraries used in this project. 

* [PDFBox](https://pdfbox.apache.org/download.html)
* [JUnit5](https://junit.org/junit5/)
* [javaFX](https://openjfx.io/)
* [SQL](https://docs.oracle.com/javase/8/docs/api/java/sql/package-summary.html)

#### Made by
- João Martins
- Maria Lopes
- Nuno Alves
<p align="right">(<a href="#top">back to top</a>)</p>
